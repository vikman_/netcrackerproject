#!/bin/sh

. /etc/os-release
# $ID - linux distribution
# $1 - Jenkins username
#      Jenkins password
# $2 - PostgreSQL database name
# $3 - PostgreSQL username
#      PostgreSQL password
# $4 - Liferay e-mail
# $5 - Liferay e-mail domain
# $6 - Liferay first name
# $7 - Liferay last name

if [ -f "params" ]; 
then
    printf "Script has already been run.\nRun \"terraform destroy\" and delete params and hosts files to run this script again.\n"
    exit 0
fi
touch params

echo "Jenkins Username: $1"
echo "PostgreSQL Username: $3"
echo "Liferay E-mail: $4@$5"
echo "Liferay First Name and Last Name: $6 $7"

postgresql_passwd="default"
jenkins_passwd="default"

echo "postgresql_db_name:" >> params
echo "$2" >> params
echo "postgresql_usr:" >> params
echo "$3" >> params
echo "postgresql_passwd:" >> params
echo "$postgresql_passwd" >> params
echo "liferay_email:" >> params
echo "$4" >> params
echo "liferay_email_domain:" >> params
echo "$5" >> params
echo "liferay_first_name:" >> params
echo "$6" >> params
echo "liferay_last_name:" >> params
echo "$7" >> params

echo "Your OS is $ID"
echo "Installing Ansible..."

if [ "$ID" = "centos" ]
then
    sudo yum install epel-release -y > /dev/null
    sudo yum install ansible -y > /dev/null
    sudo yum install unzip -y > /dev/null
elif [ "$ID" = "ubuntu" ]
then
    sudo apt-get update > /dev/null
    sudo apt-get install software-properties-common -y > /dev/null
    sudo apt-add-repository --yes --update ppa:ansible/ansible > /dev/null
    sudo apt-get install ansible -y > /dev/null
    sudo apt-get install unzip -y > /dev/null
fi

echo "Installing Terraform..."

curl -O https://hashicorp-releases.website.yandexcloud.net/terraform/1.1.9/terraform_1.1.9_linux_amd64.zip
unzip terraform_1.1.9_linux_amd64.zip
sudo mv terraform /usr/local/bin/
rm terraform_1.1.9_linux_amd64.zip

echo "Creating Terraform machines..."
terraform init > /dev/null
terraform apply -auto-approve > /dev/null
echo "Terraform machines created."

rm -f hosts
touch hosts
printf "[liferay]\n" >> hosts
liferay_ip=$(terraform output -raw external_ip_address_vm_1)
echo "$liferay_ip ansible_ssh_user=centos" >> hosts

printf "[postgresql]\n" >> hosts
postgre_ip=$(terraform output -raw external_ip_address_vm_2)
echo "$postgre_ip ansible_ssh_user=centos" >> hosts
echo "postgre_ip:" >> params
echo "$postgre_ip" >> params

printf "[jenkins]\n" >> hosts
jenkins_ip=$(terraform output -raw external_ip_address_vm_3)
echo "$jenkins_ip ansible_ssh_user=centos" >> hosts
echo "Hosts file for Ansible created."

export ANSIBLE_HOST_KEY_CHECKING=False
echo "Environment variables set."

ansible-playbook playbooks/jenkins.yml -i ./hosts --extra-vars "jenkins_username=$1 jenkins_password=$jenkins_passwd posgresql_db_name=$2 posgresql_username=$3 posgresql_password=$postgresql_passwd liferay_email=$4 liferay_email_domain=$5 liferay_first_name=$6 liferay_last_name=$7 liferay_ip=$liferay_ip postgre_ip=$postgre_ip"
echo "Jenkins installed and started at $jenkins_ip:8080/jenkins."
echo "Liferay will start at $liferay_ip:8080 in several minutes."
