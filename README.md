# NetCracker Course Project


## Description:
This project deploys cloud infrastructure on the YandexCloud platform.

### Scheme:
![image.png](./image.png)

## Prepare script
In file `main.tf` change YandexCloud parametrs: `token,` `cloud_id,` `folder_id` to yours:
```
provider "yandex" {
  token     = "<OAuth>"
  cloud_id  = "<cloud identifier>"
  folder_id = "<folder identifier>"
  zone      = "ru-central1-b"
}
```
## Start Deployment
0. Generate ssh key with ssh-keygen
1. Clone git repository to client machine
2. Change main.tf so that is contains your Yandex.Cloud tokens (see "Prepare script")
3. Change default Jenkins and PostgreSQL passwords
4. Run `init.sh` script with the parameters:

```bash
./init.sh <jenkins_username> <db_name> <postgresql_username> <mail> <mail_domain> <liferay_first_name> <liferay_last_name>
```
<hr>
* Jenkins jobs [repository](https://gitlab.com/vikman_/jenkins_jobs)
